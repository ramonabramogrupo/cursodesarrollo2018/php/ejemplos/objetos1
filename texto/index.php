
<?php
function __autoload($n) {
    include $n . '.php';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $texto=new Cadena("Ejemplo");
        echo "La frase introducida es " . $texto->getValor();
        echo "<br>";
        echo "La longitud es " . $texto->getLongitud();
        echo "<br>";
        echo "El numero de vocales es " . $texto->getVocales();
        echo "<br>";
        echo "La frase introducida es " . $texto->getValor(true);
        ?>
    </body>
</html>
